const controller = {};
const fs = require('fs');
const dataFile = 'data.json';

controller.shortenUrl = async (req, res) => {
	try {
		const data = controller.getData(dataFile);
		const index = data.length;
		const {longUrl} = req.query;

		let output = [];
		let final;
		
		for (let i = 0; i <= 9999; i++) {
			final = (`000${i}`).slice(-4);

			output.push(final);
		}

		const source = {id: output[index], shortUrl: `https://localhost:3000/${output[index]}`, longUrl: longUrl};
		data.push(source);

		controller.updateData(data);

		return res.status(200).json({code: `01`, status: true, data: data});
	} catch (error) {
		return res.status(400).json({code: `02`, status: false, data: `Error occurred, reason: ${error.message}`});
	}
};

controller.getId = async (req, res) => {
	try {
		const data = controller.getData(dataFile);
		const {id} = req.params;

		const getDataById = data.filter(e => e.id == id);

		if (getDataById.length > 0) {
			const url = getDataById[0].longUrl;

			return res.redirect(url);
		} else {
			return res.status(400).json({code: `02`, status: false, data: `Url with that ID cannot be found.`});
		}
	} catch (error) {
		return res.status(400).json({code: `02`, status: false, data: `Error occurred, reason: ${error.message}`});
	}
};

controller.getData = () => {
	const getData = fs.readFileSync(dataFile);

	return JSON.parse(getData);
};

controller.updateData = (data) => {
	fs.writeFileSync(dataFile, JSON.stringify(data, null, 4));

	console.log(`Data updated successfully.`);
};

module.exports = controller;
const express = require('express');
const router = express.Router();
const controller = require('../controller');

router.post('/shortenUrl', controller.shortenUrl);
router.get('/:id', controller.getId);

module.exports = router;